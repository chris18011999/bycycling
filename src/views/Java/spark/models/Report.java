package spark.models;

import java.util.ArrayList;

/**
 * Created by gonzovilla89 on 06/06/16.
 */
public final class Report {
    private ArrayList<Employee> employeesData;
    private String startDate;
    private String endDate;
    private boolean isBudgetSet;

    public Report (String startDate){
        this.startDate = startDate;
        this.employeesData = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeesData() {
        return this.employeesData;
    }

    public void setEmployeesData(ArrayList<Employee> employeesData) {
        this.employeesData = employeesData;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isBudgetSet() { return this.isBudgetSet; }

    public void setIsBudgetSet(boolean isBudgetSet) { this.isBudgetSet = isBudgetSet; }
}
