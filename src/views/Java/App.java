import com.mongodb.BasicDBObject;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import spark.ModelAndView;
import spark.Utils.*;
import spark.models.Company;
import spark.models.Report;
import spark.template.velocity.VelocityTemplateEngine;
import spark.Utils.MongoHelper;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class App {

    public static void main(String[] args) {
        setServerPort();
        staticFileLocation("/public");
        //BackgroundService.taskEveryXHoursAfter(24, 0);

        /*
        System.out.println("PolylineDecoder");
        String codedString0 = "a{v|HyqkYoViWuAy@sAmAuAaAsAq@}AH}Ah@wAh@wA^aDtC_BFwAvAwAjAqAxA}@r@mAnA{A~@oAjAuAdAuAjAwA`AiAAmA_BwBgF";
        String codedString1 = "{u~|Hmk_YFEVsCe@m@_AuBcAkBeAeBf@xAf@bA\\ZdAlBbAbB_@pBs@l@DDjAyAYkCaAwBeAmBo@gA";
        String codedString2 = "kty|Hu`lYyAl[gAnBgAfBe@z@DtCw@`CgAnBeAdBs@zBw@rBw@zBkAtAeAbBg@hCaAlBe@jAAvC_@jCeAnB_AjB}@lBMHoAnAgA`BmArA_EJoAzAoAdAyAz@qAhAoApAoAjAUV_@b@oAlAoAfAoA~AaBb@mArAi@zAoAlAqApA{@rBt@vBh@dCP\\j@nCFpCWtCSrCGlB?BmAbDsBhFeApBiAvAoAtAo@t@gAbBkA|AkA`BqA~AkA~AiA|Ao@~@oAzAsAhBgAzAqAvAkAvAiA|AcA|Ae@r@kArAkAtAgAhBgA|AS\\kA|AmA~AqAlA_AdBmAvAcAfAkAtAiA~AmAlAgAjBoAfAg@p@iAzAkAvAkAvAgA|AV|@v@`C\\rCd@fCDxC@zCJ`BFtCc@tCDrCTlDHdAT`DVzCu@|BcAdBk@jCa@hCHn@`AfBdAjBp@xBrApAfAxADFbAbBdAhBbAd";
        String codedString3 = "gx_}Hq}aYlMjO|AUjAcBLg@XoCzAVvAfAnApAFFxAr@`BTzA^rAbA~AfAz@x@vApAdA|AzA@jAqAvAyAl@m@FGlAmAdDcDnAqAfAwB~@eC|@_Cb@kAv@qBx@uB|@_CrAoBpAaAbBw@|@_@|As@zAq@zAq@bB_@rAcAtAy@vA_ApAiAtAsAnA{AnAaBhAgB`AaBf@u@lAgBxA{BxA{BhAeBfAeBbAoBNYrEwHhAyAFEjAuA|@kBfAgBfA{AlAaBdA}AlAwA~@gBlAmA|@uBzA}@~A_@nAy@zAeApAiAnAmAtA}@b@gCQ{BVoC`AmBpA}@dDoCnAgAZ[lAqA~AV|ACnAcBnAkA^]~AyArAoAtAoAvAqAn@m@rAoArAoAnAoAdA_B|@qBd@mAz@qBTqC`@uCF_@d@kC\\sCb@eCb@oCZsBBKp@iC|@yCfAgBfAgBnAsAhAyAx@qBr@iCFuCJuCL}Ct@wBr@y@pAmAfBcBlAgArAmADEOsCi@oC";
        String unescapedString = StringEscapeUtils.unescapeJava(codedString0);
        System.out.println(unescapedString);
        System.out.println(PolylineDecoder.decode(unescapedString, 1E5));
        */

        get("/", (request, response) -> {
            response.redirect("/login");
            return "";
        });

        get("/login", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            return new ModelAndView(model, "templates/login.vtl");
        }, new VelocityTemplateEngine());

        post("/login", (request, response) -> {

            MongoHelper mongoH = MongoHelper.getInstance();

            final String email = request.queryParams("email");
            final String password = request.queryParams("password");

            if (mongoH.authCompany(email, password)) {
                request.session().attribute("user", email);
                response.redirect("/dashboard");
            } else {
                response.redirect("/");
                halt(401, "Not authorized");
            }
            return "";
        });

        get("/register", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            return new ModelAndView(model, "templates/register.vtl");
        }, new VelocityTemplateEngine());

        post("/register", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            BasicDBObject companyData = new BasicDBObject();
            BasicDBObject addressData = new BasicDBObject();
            BasicDBObject location = new BasicDBObject();

            String email = request.queryParams("email");

            String street = request.queryParams("street");
            String number = request.queryParams("number");
            String city = request.queryParams("city");
            String country = request.queryParams("country");
            String zipCode = request.queryParams("code");

            try {
                String[] latLon = Geocoder.getLatLongPositions(street + " " + number + " " + city + " " + zipCode + " " + country);
                if (latLon != null) {
                    location.put("latitude", latLon[0]);
                    location.put("longitude", latLon[1]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            location.put("street", street);
            location.put("number", number);
            location.put("city", city);
            location.put("country", country);
            location.put("zipCode", zipCode);

            companyData.put("name", request.queryParams("name"));
            companyData.put("email", email);
            companyData.put("domain", "@" + request.queryParams("domain"));
            companyData.put("startDate", request.queryParams("date"));

            BasicDBObject password = new BasicDBObject();
            byte[] salt = Passwords.getNextSalt();
            if (request.queryParams("password") != null) {
                byte[] hashPass = Passwords.hash(request.queryParams("password").toCharArray(), salt);
                password.put("hash", Base64.encodeBase64String(hashPass));
                password.put("salt", Base64.encodeBase64String(salt));
                companyData.put("password", password);
            }

            addressData.put("locations", location);
            companyData.put("budget", "");

            boolean captcha = VerifyCaptcha.verify(request.queryParams("g-recaptcha-response"));
            if (!captcha) {
                request.session().attribute("errorMsg", "Captcha not verified");
                response.redirect("/error");
                halt(401, "Captcha not verified");
            }
            if (!mongoH.companyExists(email)) {
                mongoH.saveCompany(companyData, email, addressData);
                request.session().attribute("user", email);
                response.redirect("/dashboard");
            } else {
                request.session().attribute("errorMsg", "Email already registered");
                response.redirect("/error");
                halt(401, "Email already registered");
            }
            return "";
        });

        get("/error", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            String errorMsg = request.session().attribute("errorMsg");
            request.session().removeAttribute("errorMsg");
            model.put("message", errorMsg);
            return new ModelAndView(model, "templates/error.vtl");
        }, new VelocityTemplateEngine());

        before("/dashboard", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String session = request.session().attribute("user");
            if (!(session != null && mongoH.isSessionActive(session))) {
                response.redirect("/");
                halt(401, "Not authorized");
            }
        });

        get("/dashboard", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            Map<String, Object> model = new HashMap<>();
            String email = request.session().attribute("user");
            if (mongoH.hasValidLocation(email)) {
                model.put("email", email);
                Report report = mongoH.getReport(email);
                model.put("usersData", report.getEmployeesData());
                model.put("startDate", report.getStartDate());
                model.put("endDate", report.getEndDate());
                model.put("budgetSet", report.isBudgetSet());
            } else {
                model.put("email", "Company address not valid");
            }
            return new ModelAndView(model, "templates/dashboard.vtl");
        }, new VelocityTemplateEngine());

        before("/settings", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String session = request.session().attribute("user");
            if (!(session != null && mongoH.isSessionActive(session))) {
                response.redirect("/");
                halt(401, "Not authorized");
            }
        });

        get("/settings", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            Company companyData = mongoH.getCompanyFull(request.session().attribute("user"));
            Map<String, Object> model = new HashMap<>();
            String email = request.session().attribute("user");
            if (mongoH.hasValidLocation(email)) {
                model.put("email", email);
            } else {
                model.put("email", "Company address not valid");
            }
            model.put("company", companyData);
            if (!(companyData.getBudget()).equals("")){
                double budget = Double.parseDouble(companyData.getBudget());
                double perKmDouble = mongoH.calculateEarningsPerKm(budget);
                String perKM = "€&nbsp;" + Double.toString((double) Math.round(perKmDouble * 10) / 10);
                model.put("perKm", perKM);
            }
            return new ModelAndView(model, "templates/settings.vtl");
        }, new VelocityTemplateEngine());

        post("/update/info", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            BasicDBObject companyData = new BasicDBObject();
            BasicDBObject addressData = new BasicDBObject();
            BasicDBObject location = new BasicDBObject();

            String email = request.session().attribute("user");

            String street = request.queryParams("street");
            String number = request.queryParams("number");
            String city = request.queryParams("city");
            String country = request.queryParams("country");
            String zipCode = request.queryParams("code");

            try {
                String[] latLon = Geocoder.getLatLongPositions(street + " " + number + " " + city + " " + zipCode + " " + country);
                if (latLon != null) {
                    location.put("latitude", latLon[0]);
                    location.put("longitude", latLon[1]);
                } else {
                    location.put("latitude", "");
                    location.put("longitude", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            location.put("street", street);
            location.put("number", number);
            location.put("city", city);
            location.put("country", country);
            location.put("zipCode", zipCode);

            companyData.put("name", request.queryParams("name"));
            String newEmail = request.queryParams("email");
            companyData.put("email", newEmail);
            companyData.put("domain", "@" + request.queryParams("domain"));

            addressData.put("locations", location);
            mongoH.updateCompanyInfo(email, companyData);
            mongoH.updateCompanyAddress(email, addressData);
            request.session().attribute("user", newEmail);
            response.redirect("/settings");
            return "";
        });

        post("/update/password", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String email = request.session().attribute("user");
            if (mongoH.authCompany(email, request.queryParams("currentPassword"))) {
                byte[] salt = Passwords.getNextSalt();
                byte[] hashPass = Passwords.hash(request.queryParams("newPassword").toCharArray(), salt);
                BasicDBObject password = new BasicDBObject();
                BasicDBObject companyData = new BasicDBObject();
                password.put("hash", Base64.encodeBase64String(hashPass));
                password.put("salt", Base64.encodeBase64String(salt));
                companyData.put("password", password);
                mongoH.updateCompanyInfo(email, companyData);
            } else {
                request.session().attribute("errorMsg", "Incorrect current password");
                response.redirect("/error");
            }
            response.redirect("/settings");
            return "";
        });

        post("/update/incentives", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String email = request.session().attribute("user");
            BasicDBObject companyData = new BasicDBObject();
            companyData.put("budget", request.queryParams("budget"));
            companyData.put("startDate", request.queryParams("date"));
            mongoH.updateCompanyInfo(email, companyData);
            response.redirect("/settings");
            return "";
        });

        //get("/download", (request, response) -> DownloadHelper.getFile(request,response));

        before("/feedback", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String session = request.session().attribute("user");
            if (!(session != null && mongoH.isSessionActive(session))) {
                response.redirect("/");
                halt(401, "Not authorized");
            }
        });

        get("/feedback", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            Map<String, Object> model = new HashMap<>();
            String email = request.session().attribute("user");
            if (mongoH.hasValidLocation(email)) {
                model.put("email", email);
            } else {
                model.put("email", "Company address not valid");
            }
            return new ModelAndView(model, "templates/feedback.vtl");
        }, new VelocityTemplateEngine());

        post("/feedback", (request, response) -> {
            String issue = request.queryParams("issue");
            String message = request.queryParams("message");
            String email = request.session().attribute("user");
            EmailHelper.sendEmail("Issue:\n" + issue + "\n\nMessage:\n" + message + "\n\nEmail:\n" +  email);
            response.redirect("/dashboard");
            return "";
        });

        post("/logout", (request, response) -> {
            request.session().removeAttribute("user");
            response.redirect("/");
            return "";
        });

        post("/user", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            boolean mongoSaved = mongoH.saveUser(request.body());
            response.type("application/json");
            if (mongoSaved) {
                return "{\"updated\":true}";
            } else {
                return "{\"updated\":false}";
            }
        });

        post("/rides/:userId", (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            boolean mongoSaved = mongoH.saveRides(request.params(":userId"), request.body());
            response.type("application/json");
            if (mongoSaved) {
                return "{\"updated\":true}";
            } else {
                return "{\"updated\":false}";
            }
        });

        post("/work_email/:userId", (request, response) -> {
            EmailHelper.sendNewWorkEmail(request.body());
            MongoHelper mongoH = MongoHelper.getInstance();
            boolean mongoSaved = mongoH.updateWorkEmail(request.params(":userId"), request.body());
            response.type("application/json");
            if (mongoSaved) {
                return "{\"updated\":true}";
            } else {
                return "{\"updated\":false}";
            }
        });

        post("/logs/:log", (request, response) -> {
            System.out.println((request.params(":log") + request.body()).replaceAll("\\s+",""));
            return "";
        });

        get("/leaderboard/:userId",  (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String userId = request.params(":userId");
            JSONObject leaderboard = mongoH.getLeaderboard(userId, 1);
            response.type("application/json");
            return leaderboard;
        });

        get("/v2/leaderboard/:userId",  (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String userId = request.params(":userId");
            JSONObject leaderboard = mongoH.getLeaderboard(userId, 2);
            response.type("application/json");
            return leaderboard;
        });

        get("/global_variables",  (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            JSONObject variables = mongoH.getGlobalVariables();
            response.type("application/json");
            return variables;
        });

        get("/sendLeaderboards/:domain",  (request, response) -> {
            MongoHelper mongoH = MongoHelper.getInstance();
            String domain = "";
            domain = request.params(":domain");
            String emailBody = mongoH.getStringFormattedLeaderboard(domain);
            EmailHelper.sendLeaderboardsEmail(emailBody);
            //EmailHelper.sendLeaderboards("Bold: *here* \nNew line");
            //String leaderboard = mongoH.getStringFormattedLeaderboard(domain);
            //response.type("text/html");
            //return leaderboard;
            response.type("text/html");
            return emailBody;
        });

        /*
        get("/general_leaderboard/:domain",  (request, response) -> {
            MongoHelper mongoH = new MongoHelper();
            String domain = request.params(":domain");
            String leaderboard = mongoH.getStringFormattedLeaderboard(domain);
            response.type("text/xml");
            return leaderboard;
        });


        get("/sendLeaderboards",  (request, response) -> {
            //EmailHelper.sendLeaderboards("Bold: *here* \nNew line");
            //String leaderboard = mongoH.getStringFormattedLeaderboard(domain);
            //response.type("text/html");
            //return leaderboard;
            return "";
        });
        */

        get("/404", (request, response) -> {
            return new ModelAndView(new HashMap(), "templates/404.vtl");
        }, new VelocityTemplateEngine());

        get("/*", (request, response) -> {
            throw new NotFoundException();
        });

        exception(NotFoundException.class, (e, request, response) -> response.redirect("/404"));

    }


    public static void setServerPort() {
        ProcessBuilder process = new ProcessBuilder();
        Integer port;
        if (process.environment().get("PORT") != null) {
            port = Integer.parseInt(process.environment().get("PORT"));
        } else {
            port = 4567;
        }
        port(port);
    }

}
