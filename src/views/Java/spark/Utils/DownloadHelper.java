package spark.Utils;

import spark.Request;
import spark.Response;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by gonzovilla89 on 16/06/16.
 */
public class DownloadHelper {

    public static File getFile(Request request, Response response) {

        File file = null;
        StringBuilder sb = null;
        try {
            file = new File("report.csv");
            sb = new StringBuilder();
            sb.append("id");
            sb.append(',');
            sb.append("Name");
            sb.append('\n');

            sb.append("1");
            sb.append(',');
            sb.append("Prashant Ghimire");
            sb.append('\n');

            response.raw().setContentType("text/csv");
            response.raw().setHeader("Content-Disposition", "attachment; filename=" + file.getName());
            response.raw().getOutputStream().write(sb.toString().getBytes());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
