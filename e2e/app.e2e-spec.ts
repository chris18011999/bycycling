import { BycyclingPage } from './app.po';

describe('bycycling App', () => {
  let page: BycyclingPage;

  beforeEach(() => {
    page = new BycyclingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
