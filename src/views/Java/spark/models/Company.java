package spark.models;

public final class Company {
    private String name;
    private String email;
    private String domain;
    private String date;
    private String street;
    private int number;
    private String city;
    private String country;
    private String code;
    private String budget;

    public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name.replaceAll(" ", "&nbsp;");
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDomain() {
        return this.domain.replaceAll(" ", "&nbsp;").replaceAll("@", "");
    }

    public void setDomain(String domain) {
        this.domain = "@" + domain;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStreet() {
        return this.street.replaceAll(" ", "&nbsp;");
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return this.number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getCity() {
        return this.city.replaceAll(" ", "&nbsp;");
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return this.country.replaceAll(" ", "&nbsp;");
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBudget() {
        return this.budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }
}
