package spark.Utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
/**
 * Created by gonzovilla89 on 02/06/16.
 */
public class DateHelper {

    public static String getStartDate(String stringStartDate){
        Date currentDate = new Date();
        Date startDate = stringToDate(stringStartDate);

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        int currentDay = cal.get(Calendar.DAY_OF_MONTH);
        int currentMonth = cal.get(Calendar.MONTH);
        int currentYear = cal.get(Calendar.YEAR);
        cal.setTime(startDate);
        int startDay = cal.get(Calendar.DAY_OF_MONTH);

        if (startDate.after(currentDate)){
            return stringStartDate;
        }else if (startDay <= currentDay) {
            return String.format("%04d-%02d-%02d", currentYear, currentMonth+1, startDay);
        } else if (currentMonth == 0) {
            return String.format("%04d-%02d-%02d", currentYear-1, 12, startDay);
        } else if (currentMonth == 2 && startDay > 28){
            return String.format("%04d-%02d-%02d", currentYear, currentMonth, 28);
        } else if ((currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) && startDay > 30) {
            return String.format("%04d-%02d-%02d", currentYear, currentMonth, 30);
        } else {
            return String.format("%04d-%02d-%02d", currentYear, currentMonth, startDay);
        }
    }

    private static Date stringToDate (String stringDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormat.parse(stringDate);
        } catch (ParseException e) {
            System.out.println("Unparseable using " + dateFormat);
        }
        return date;
    }

    public static String dateToString (Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    public static String dateToFormatString (Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat ("d MMMMM yyyy");
        return dateFormat.format(date);
    }

    public static String stringToFormatString (String stringDate){
        Date date = stringToDate(stringDate);
        return dateToFormatString(date);
    }

    public static String dateToTimeString (int timeType, String dateString){
        switch (timeType){
            case Constants.DAY:
                SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
                return dateFormat.format(stringToDate(dateString));
            case Constants.WEEK:
                Calendar cal = Calendar.getInstance();
                cal.setFirstDayOfWeek(Calendar.MONDAY);
                cal.setTime(stringToDate(dateString));
                int week = cal.get(Calendar.WEEK_OF_YEAR);
                int year = cal.get(Calendar.YEAR);
                return String.format("%04d-%02d", year, week);
            case Constants.MONTH:
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(stringToDate(dateString));
                int month = calendar.get(Calendar.MONTH) + 1;
                int year2 = calendar.get(Calendar.YEAR);
                return String.format("%04d-%02d", year2, month);
            default:
                return "";
        }
    }

    public static boolean belongsToReport(String rideDate, String startDate){
        Date ride = stringToDate(rideDate);
        Date start = stringToDate(startDate);
        Date current = new Date();
        return !ride.before(start) && !ride.after(current);
    }
}
