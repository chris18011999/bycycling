package spark.Utils;

/**
 * Created by gonzovilla89 on 07/06/16.
 */
public final class Constants {
    public static final int DAY = 0;
    public static final int WEEK = 1;
    public static final int MONTH = 2;
    public static final double AVG_COMMUTE_KM = 15.0;
    public static final int USERS_ABOVE = 4;
    public static final int USERS_BELOW = 4;
    public static final int USERS_ABOVE_V2 = 200;
    public static final int USERS_BELOW_V2 = 200;

}
