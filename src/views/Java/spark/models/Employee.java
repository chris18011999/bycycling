package spark.models;

import static java.lang.Math.toIntExact;

public final class Employee {
    private String name;
    private String email;
    private double distance;
    private double time;
    private int rides;
    private double earnings;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name.replaceAll(" ", "&nbsp;");
    }

    public String getEmail() {
        return this.email;
    }

    public double getDistance() {
        return this.distance;
    }

    public String getFormatDistance(){
        String stringDistance = Double.toString((double) Math.round((this.distance/1000) * 10) / 10);
        return stringDistance + "&nbsp;km";
    }

    public double getTime() {
        return this.time;
    }

    public String getFormatTime(){
        double hours = 0.0;
        double minutes = 0.0;
        String stringTime = "";
        if (this.time > 3600.0){
            hours = this.time / 3600.0;
        }
        minutes = (this.time % 3600.0)/60.0;
        if (hours > 0.0) {
            stringTime = Integer.toString(toIntExact(Math.round(hours))) + "&nbsp;h&nbsp;" + Integer.toString(toIntExact(Math.round(minutes))) + "&nbsp;min";
        } else {
            stringTime = Integer.toString(toIntExact(Math.round(minutes))) + "&nbsp;min";
        }
        return stringTime;
    }

    public int getRides() {
        return this.rides;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public void setRides(int rides) {
        this.rides = rides;
    }

    public double getEarnings() { return this.earnings; }

    public String getFormatEarnings() {
        String stringEarnings = Double.toString((double) Math.round(this.earnings * 10) / 10);
        return "€&nbsp;" + stringEarnings;
    }

    public void setEarnings(double earnings) { this.earnings = earnings; }
}
